module jwt

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gofiber/fiber/v2 v2.1.0
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.2.0
)
