package main
import (
        "fmt"
        "net/http"
        "io/ioutil"
        _ "github.com/go-sql-driver/mysql"
        "github.com/gorilla/mux"
        "encoding/json"
)
type Result struct {
        Deployer string
        FilmDetail Film
}
type Film struct {
	Title string    
	Year string    
	Genre string  
	Released string
	Writer string
	ImdbRating string  
}
type Product []struct {
        Sku         string `json:"Sku"`
        ProductName string `json:"Product_name"`
        Stocks      int    `json:"Stocks"`
}

func hello(w http.ResponseWriter, req *http.Request) {
        fmt.Fprintf(w, "Hello Praditya Lutvi Charisma\n")
}

func getDataServer1(w http.ResponseWriter, r *http.Request){
        url := "http://3.93.63.220:9000/getFilmDetail"
        req, _ := http.NewRequest("GET", url, nil)
        res, _ := http.DefaultClient.Do(req)
        defer res.Body.Close()
        body, _ := ioutil.ReadAll(res.Body)
    
        //fmt.Println(res)
        //fmt.Println(string(body))
        var result Result
        json.Unmarshal(body, &result)
        json.NewEncoder(w).Encode(result)
}

func getDataServer2(w http.ResponseWriter, r *http.Request){
        url := "http://3.93.63.220:8090/getProduct"
        req, _ := http.NewRequest("GET", url, nil)
        res, _ := http.DefaultClient.Do(req)
        defer res.Body.Close()
        body, _ := ioutil.ReadAll(res.Body)
        
        //fmt.Println(res)
        //fmt.Println(string(body))
        var products Product
        json.Unmarshal(body, &products)
        json.NewEncoder(w).Encode(products)
}
func main() {
        router := mux.NewRouter()
        router.HandleFunc("/getDataServer1",getDataServer1).Methods("GET") // menjalurkan URL untuk dapat mengkases data JSON API Film
        router.HandleFunc("/",hello)
        router.HandleFunc("/getDataServer2",getDataServer2).Methods("GET") // menjalurkan URL untuk dapat mengkases data JSON API Db Product
        // http.HandleFunc("/", hello)
        fmt.Println(`listen at :8000`)
        fmt.Println(http.ListenAndServe(":8000", router))
}
