package main

import (
	"fmt"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
)

type FilmAPI struct {
	Title      string    `json:"Title"`
	Year       string    `json:"Year"`
	Rated      string    `json:"Rated"`
	Released   string    `json:"Released"`
	Runtime    string    `json:"Runtime"`
	Genre      string    `json:"Genre"`
	Director   string    `json:"Director"`
	Writer     string    `json:"Writer"`
	Actors     string    `json:"Actors"`
	Plot       string    `json:"Plot"`
	Language   string    `json:"Language"`
	Country    string    `json:"Country"`
	Awards     string    `json:"Awards"`
	Poster     string    `json:"Poster"`
	Ratings    []Ratings `json:"Ratings"`
	Metascore  string    `json:"Metascore"`
	ImdbRating string    `json:"imdbRating"`
	ImdbVotes  string    `json:"imdbVotes"`
	ImdbID     string    `json:"imdbID"`
	Type       string    `json:"Type"`
	DVD        string    `json:"DVD"`
	BoxOffice  string    `json:"BoxOffice"`
	Production string    `json:"Production"`
	Website    string    `json:"Website"`
	Response   string    `json:"Response"`
}
type Ratings struct {
	Source string `json:"Source"`
	Value  string `json:"Value"`
}

func getAPI() (string,string,string,string,string, string){
	// api_key := "8ed04aa8";
	url := "http://www.omdbapi.com/?t=harry&apikey=8ed04aa8";
	req, _ := http.NewRequest("GET", url, nil);
	// req.Header.Add("apikey", api_key);
	res, _ := http.DefaultClient.Do(req);
	defer res.Body.Close();
	body, _ := ioutil.ReadAll(res.Body);
	
	var film FilmAPI
	json.Unmarshal(body, &film)
	var title = film.Title
	var year = film.Year
	var genre = film.Genre
	var released = film.Released;
	var writer = film.Writer;
	var imdbRating = film.ImdbRating;
	return title, year, genre, released, writer, imdbRating
}

type Film struct {
	Title string    
	Year string    
	Genre string  
	Released string
	Writer string
	ImdbRating string  
}

type JsonOutput struct {
	Deployer string
	FilmDetail Film
}

func getFilmDetail(w http.ResponseWriter, r *http.Request){
	var film Film // variable untuk memetakan data product yang terbagi menjadi 3 field
	title, year, genre, released, writer, imdbRating := getAPI()

	// fmt.Println("Judul ",title)
	film.Title = title
	film.Year = year
	film.Genre = genre
	film.Released = released
	film.Writer = writer
	film.ImdbRating = imdbRating

	var result JsonOutput
	result.Deployer = "Praditya Lutvi Charisma"
	result.FilmDetail = film
	json.NewEncoder(w).Encode(result)
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/getFilmDetail",getFilmDetail).Methods("GET") // menjalurkan URL untuk dapat mengkases data JSON API product ke /getproducts
	http.Handle("/",router)
	fmt.Println(`listen at :9000`)
	log.Fatal(http.ListenAndServe(":9000",router))
}